<?php
namespace Craft;

return [
    'endpoints' => [
	 'api/cars.json' => [
            'elementType' => ElementType::Entry,
            'criteria' => ['section' => 'cars'],
  			'paginate' => false,
            'transformer' => function(EntryModel $entry) {

//hotspots	
//////////////////////////////////////////////////////////////////////////////////
	$hotspots_exterior = [];
	$hotspots_interior = [];
//Content
//////////////////////////////////////////////////////////////////////////////////
			$content_en = [];
			foreach ($entry->exteriorHotspot as $block) {
				$content_images=[];
			 	foreach ($block->images as $image) {
					$content_images[]=['url' =>$image->getUrl('image1920x1080'), 'filename' =>$image->filename];
			        }
				$content_en[] = [
					'id' => $block->id, 
					'type' =>'exterior',
					'title' => $block->heading, 
					'body' => $block->body,   
					'video' => $block->video,    
					'x' => $block->point['leftPercentage'],
					'y' => $block->point['topPercentage'],
					'images'=> $content_images,
					];
					
					$hotspots_exterior[] = [
						'id' => $block->id, 
						'x' => $block->point['leftPercentage'],
						'y' => $block->point['topPercentage'],
					];
					
			};
			
			$content_interior_en = [];
			foreach ($entry->interiorHotspot as $block) {
				$content_en[] = [
					'id' => $block->id, 
					'type' =>'interior',
					'title' => $block->heading, 
					'body' => $block->body,   
					'video' => $block->video,    
					'x' => $block->point['leftPercentage'],
					'y' => $block->point['topPercentage'],
					];
			};
			$content = [];	
			$content[]=['en'=>$content_en];

//////////////////////////////////////////////////////////////////////////////////

		$navLogo = $entry->navLogo->first(); 
		$screensaverLogo = $entry->screensaverLogo->first(); 

	 	$navLogoImage[] = ['url' => $navLogo->getUrl('navLogo'), 'filename' => $navLogo->filename];
		$screensaverImage[] = ['url' => $screensaverLogo->getUrl('screensaverLogo'), 'filename' => $screensaverLogo->filename];
		
		
                return [
					'id' => $entry->id,
					'url' => $entry->url,
					'slug' => $entry->slug,
	                'jsonUrl' => UrlHelper::getUrl("api/cars/{$entry->id}.json"),
	 				'date_published' => $entry->postDate->format(\DateTime::ATOM),
					'enabled' => $entry->enabled,
	                //'title_en' => $entry->locale('en')->title,//Language version
					'title_en' => $entry->title,
					'intro_title_en' => $entry->introTitle,
					'intro_body_en' => (string)$entry->introBody,
					'intro_video_en' => $entry->introVideo,
					
					
					'screensaver_title_en' => $entry->introBody,
					'screensaver_body_en' => $entry->introBody,
					
					'logo' => $navLogoImage,
					'logo_screensaver' => $screensaverImage,
					
					'demo_exterior' => $entry->id,
					'demo_interior' => $entry->id,
					
					
					'exterior'=>$hotspots_exterior,
					'interior'=>$hotspots_interior,
					'content'=>$content,
                ];
            },
        ],
		'api/about.json' => [
	          	'elementType' => ElementType::Entry,
            	'criteria' => ['section' => 'about'],
  				'paginate' => false,
            	'transformer' => function(EntryModel $entry) {

	            return [
	           			'id' => $entry->id,
						'url' => $entry->url,
						'slug' => $entry->slug,
		                'jsonUrl' => UrlHelper::getUrl("api/privacy/{$entry->id}.json"),
		 				'date_published' => $entry->postDate->format(\DateTime::ATOM),
						'enabled' => $entry->enabled,
						'title_en' => $entry->title,
						'heading_en' => $entry->pageHeading,
						'body_en' => (string)$entry->pageBody,
						'disclaimer_en' => (string)$entry->disclaimer,
			
	            ];
	        },
	    ],
		'api/privacy.json' => [
	          	'elementType' => ElementType::Entry,
            	'criteria' => ['section' => 'privacy'],
  				'paginate' => false,
            	'transformer' => function(EntryModel $entry) {

	            return [
	           			'id' => $entry->id,
						'url' => $entry->url,
						'slug' => $entry->slug,
		                'jsonUrl' => UrlHelper::getUrl("api/privacy/{$entry->id}.json"),
		 				'date_published' => $entry->postDate->format(\DateTime::ATOM),
						'enabled' => $entry->enabled,
						'title_en' => $entry->title,
						'heading_en' => $entry->pageHeading,
						'body_en' => (string)$entry->privacyBody,
			
	            ];
	        },
	    ],
	

    ]
];