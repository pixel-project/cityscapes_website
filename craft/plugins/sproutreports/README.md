Sprout Reports!

Here are some online resources you might find useful:

Sprout Reports Docs
------------------------------------------------------------
Code examples, tags, common questions:
https://sprout.barrelstrengthdesign.com/docs/reports


Sprout Reports Updates
------------------------------------------------------------
https://sprout.barrelstrengthdesign.com/docs/support/changelog.html


Sprout Reports Support
------------------------------------------------------------

Via Craft Stack Exchange: Tag your questions with `plugin-sproutreports`:
https://craftcms.stackexchange.com/

Via Email:
Send us a note at: sprout@barrelstrengthdesign.com